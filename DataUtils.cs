﻿using System.Collections.Generic;
using System.Linq;

namespace Shihonase {
    public class DataUtils {
        public static bool CategoryBelongsToUser(long category_Id, long user_Id) {
            var output = false;
            var categories = Data.SelectCategories(new List<QueryObject>() {
                new QueryObject() {
                    IsString = false,
                    Key = "Id",
                    Value = category_Id
                }
            });
            if (categories.Any()) {
                output = categories[0].User_Id == user_Id;
            }
            return output;
        }

        public static bool TaskBelongsToCategory(long task_Id, long category_Id) {
            var output = false;
            var tasks = Data.SelectTasks(new List<QueryObject>() {
                new QueryObject() {
                    IsString = false,
                    Key = "Id",
                    Value = task_Id
                }
            });
            if (tasks.Any()) {
                output = tasks[0].Category_Id == category_Id;
            }
            return output;
        }
        
        public static bool TaskBelongsToUser(long task_Id, long user_Id) {
            var output = false;
            var tasks = Data.SelectTasks(new List<QueryObject>() {
                new QueryObject() {
                    IsString = false,
                    Key = "Id",
                    Value = task_Id
                }
            });
            if (!tasks.Any()) return false;
            var categories = Data.SelectCategories(new List<QueryObject>() {
                new QueryObject() {
                    IsString = false,
                    Key = "Id",
                    Value = tasks[0].Category_Id
                }
            });
            if (categories.Any()) {
                output = categories[0].User_Id == user_Id;
            }
            return output;
        }
    }
}