﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;

using static Shihonase.Utils;

namespace Shihonase {
    public class Data {
        public static class ConnectionData {
            public static string Pass;
            public static string User;
            public static string DbName;
            public static string Server;
        }

        public static string GetConnectionString() => $@"Data Source={ConnectionData.Server};
                Initial Catalog={ConnectionData.DbName};User ID={ConnectionData.User};Password={ConnectionData.Pass}";

        private static int ExecuteQueryNonSelect(string query) {
            var output = 0;
            using (var connection = new System.Data.SqlClient.SqlConnection(GetConnectionString())) {
                output = connection.Execute(query);
            }
            return output;
        }

        public static List<User> SelectUsers(List<QueryObject> values) {
            var output = new List<User>();
            using (var connection = new System.Data.SqlClient.SqlConnection(GetConnectionString())) {
                var query = "select * from Users ";
                var first = true;
                foreach (var v in values) {
                    var word = "and";
                    var a = "'";
                    if (!v.IsString) a = "";
                    if (first) {
                        word = "where";
                        first = false;
                    }
                    query += $"{word} {v.Key} = {a}{v.SqlEscapedValue()}{a} ";
                }
                query += ";";
                output = connection.Query<User>(query).ToList();
            }
            return output;
        }

        public static void CreateUser(User user) {
            var query = @$"insert into Users(NAME, EMAIL, PASSWORD)
                            Values('{user.Name}', '{user.Email}', '{user.Password}');";
            var output = ExecuteQueryNonSelect(query);
        }

        public static void UpdateUser(QueryObject whereValue, List<QueryObject> setValues) {
            var query = "update Users set ";
            var a = "'";
            for (var i = 0; i < setValues.Count; i++) {
                if (!setValues[i].IsString) a = ""; else a = "'";
                query += $"{setValues[i].Key} = {a}{setValues[i].SqlEscapedValue()}{a}";
                if (i < setValues.Count - 1) query += ",";
                query += " ";
            }
            a = "'";
            if (!whereValue.IsString) a = "";
            query += $"where {whereValue.Key}={a}{whereValue.SqlEscapedValue()}{a} ;";
            var output = ExecuteQueryNonSelect(query);
        }

        public static void DeleteUser(List<QueryObject> values) {
            var query = "delete from Users ";
            var first = true;
            foreach (var v in values) {
                var word = "and";
                var a = "'";
                if (!v.IsString) a = "";
                if (first) {
                    word = "where";
                    first = false;
                }
                query += $"{word} {v.Key} = {a}{v.SqlEscapedValue()}{a} ";
            }
            query += ";";
            var output = ExecuteQueryNonSelect(query);
        }
        
        public static List<Category> SelectCategories(List<QueryObject> values) {
            var output = new List<Category>();
            using (var connection = new System.Data.SqlClient.SqlConnection(GetConnectionString())) {
                var query = "select * from Categories ";
                var first = true;
                foreach (var v in values) {
                    var word = "and";
                    var a = "'";
                    if (!v.IsString) a = "";
                    if (first) {
                        word = "where";
                        first = false;
                    }
                    query += $"{word} {v.Key} = {a}{v.SqlEscapedValue()}{a} ";
                }
                query += ";";
                output = connection.Query<Category>(query).ToList();
            }
            return output;
        }

        public static void CreateCategory(Category category) {
            var current = SelectCategories(new List<QueryObject>(){
                new QueryObject() {Key = "USER_ID", Value = category.User_Id.ToString(), IsString = false}
            });
            var pos = current.Count() + 1;
            var highest = 0;
            try {highest = (from v in current select v.Position).Max(); } catch (Exception ignored) { }
            highest++;
            if (pos < highest) pos = highest;
            if (category.Position < 0) category.Position = pos;
                var query = @$"insert into Categories(USER_ID, NAME, COLOR, POSITION, DESCRIPTION)
                            Values({category.User_Id}, '{category.Name.SqlEscape()}', '{category.Color.SqlEscape()}', {category.Position}, '{category.Description.SqlEscape()}');";
                var output = ExecuteQueryNonSelect(query);
        }

        public static void UpdateCategory(QueryObject whereValue, List<QueryObject> setValues) {
            var query = "update Categories set ";
            var a = "'";
            for (var i = 0; i < setValues.Count; i++) {
                if (!setValues[i].IsString) a = ""; else a = "'";
                query += $"{setValues[i].Key} = {a}{setValues[i].SqlEscapedValue()}{a}";
                if (i < setValues.Count - 1) query += ",";
                query += " ";
            }
            a = "'";
            if (!whereValue.IsString) a = "";
            query += $"where {whereValue.Key}={a}{whereValue.SqlEscapedValue()}{a} ;";
            var output = ExecuteQueryNonSelect(query);
        }

        public static void DeleteCategory(List<QueryObject> values) {
            var query = "delete from Categories ";
            var first = true;
            foreach (var v in values) {
                var word = "and";
                var a = "'";
                if (!v.IsString) a = "";
                if (first) {
                    word = "where";
                    first = false;
                }
                query += $"{word} {v.Key} = {a}{v.SqlEscapedValue()}{a} ";
            }
            query += ";";
            var output = ExecuteQueryNonSelect(query);
        }
        
        public static List<Task> SelectTasks(List<QueryObject> values) {
            var output = new List<Task>();
            using (var connection = new System.Data.SqlClient.SqlConnection(GetConnectionString())) {
                var query = "select * from Tasks ";
                var first = true;
                foreach (var v in values) {
                    var word = "and";
                    var a = "'";
                    if (!v.IsString) a = "";
                    if (first) {
                        word = "where";
                        first = false;
                    }
                    query += $"{word} {v.Key} = {a}{v.SqlEscapedValue()}{a} ";
                }
                query += ";";
                output = connection.Query<Task>(query).ToList();
            }
            return output;
        }

        public static void CreateTask(Task task) {
            var current = SelectTasks(new List<QueryObject>(){
                new QueryObject() {Key = "CATEGORY_ID", Value = task.Category_Id.ToString(), IsString = false},
            });
            var pos = current.Count() + 1;
            var highest = 0;
            try {highest = (from v in current select v.Position).Max(); } catch (Exception ignored) { }
            highest++;
            if (pos < highest) pos = highest;
            if (task.Position < 0) task.Position = pos;
            var query = @$"insert into Tasks(CATEGORY_ID, DEADLINE, STATE, NAME, COLOR, POSITION, DESCRIPTION)
                            Values({task.Category_Id}, '{DateTimeToSmallDateTime(task.Deadline)}',
                            {task.State}, '{task.Name.SqlEscape()}', '{task.Color}', {task.Position}, '{task.Description.SqlEscape()}' );";
            var output = ExecuteQueryNonSelect(query);
        }

        public static void UpdateTask(QueryObject whereValue, List<QueryObject> setValues) {
            var query = "update Tasks set ";
            var a = "'";
            for (var i = 0; i < setValues.Count; i++) {
                if (!setValues[i].IsString) a = ""; else a = "'";
                if (!setValues[i].IsDateTime) {
                    query += $"{setValues[i].Key} = {a}{setValues[i].SqlEscapedValue()}{a}";
                }
                else {
                    query += $"{setValues[i].Key} = '{DateTimeToSmallDateTime(setValues[i].Value)}'";
                }
                if (i < setValues.Count - 1) query += ",";
                query += " ";
            }
            a = "'";
            if (!whereValue.IsString) a = "";
            query += $"where {whereValue.Key}={a}{whereValue.SqlEscapedValue()}{a} ;";
            var output = ExecuteQueryNonSelect(query);
        }

        public static void DeleteTask(List<QueryObject> values) {
            var query = "delete from Tasks ";
            var first = true;
            foreach (var v in values) {
                var word = "and";
                var a = "'";
                if (!v.IsString) a = "";
                if (first) {
                    word = "where";
                    first = false;
                }
                query += $"{word} {v.Key} = {a}{v.SqlEscapedValue()}{a} ";
            }
            query += ";";
            var output = ExecuteQueryNonSelect(query);
        }
    }

    public class User {
        public long Id{ get; set; }
        public string Name{ get; set; }
        public string Email{ get; set; }
        public string Password{ get; set; }
    }

    public class Category {
        public long Id{ get; set; }
        public long User_Id{ get; set; }
        public string Name{ get; set; }
        public string Color{ get; set; }
        public int Position{ get; set; }
        public string Description{ get; set; }
    }

    public class Task {
        public long Id{ get; set; }
        public long Category_Id{ get; set; }
        public DateTime Deadline{ get; set; }
        public sbyte State{ get; set; }
        public string Name{ get; set; }
        public string Color{ get; set; }
        public int Position{ get; set; }
        public string Description{ get; set; }
    }

    public class QueryObject {
        public string Key { get; set; }
        public dynamic Value{ get; set; }
        public bool IsString { get; set; }

        public bool IsDateTime = false;

        public QueryObject() { }

        public QueryObject(string key, string value, bool isString) {
            Key = key;
            Value = value;
            IsString = isString;
        }

        public string SqlEscapedValue() => this.Value.ToString().Replace("'", @"''");
    }
}