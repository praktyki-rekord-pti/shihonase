﻿using System.Collections.Generic;

namespace Shihonase.Controllers {

    public class DefaultResponse {
        public dynamic Response { get; set; }

        public DefaultResponse() {}

        public DefaultResponse(dynamic response) { 
            Response = response;
        }
    }

    public class LoginRequest {
        public string Email { get; set; }
        public string Password { get; set; }
    }

    public class UpdateUserRequest {
        public string Email { get; set; }
        public string Password { get; set; }
        public User User { get; set; }
    }

    public class AddCategoryRequest {
        public string Email { get; set; }
        public string Password { get; set; }
        public Category Category { get; set; }
    }
    
    public class UpdateCategoriesRequest {
        public string Email { get; set; }
        public string Password { get; set; }
        public List<Category> Categories { get; set; }
    }

    public class GetTasksRequest {
        public string Email { get; set; }
        public string Password { get; set; }
        public long Category_Id { get; set; }
    }
    
    public class AddTaskRequest {
        public string Email { get; set; }
        public string Password { get; set; }
        public Task Task { get; set; }
    }
    public class UpdateTasksRequest {
        public string Email { get; set; }
        public string Password { get; set; }
        public List<Task> Tasks { get; set; }
    }

    
}