﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

using static Shihonase.Utils;
namespace Shihonase.Controllers {
    [Route("[controller]")]
    [ApiController]
    public class UserController : ControllerBase{
        
        /// <summary>
        /// Returns response = true if email&password are correct
        /// Returns response = false if email&password are incorrect
        /// Returns response = false if body is not correct
        /// Returns response = true if user was just created
        /// </summary>
        [HttpPost("/api/login")]
        public string OnLogin([FromBody] object body) {
            var output = new DefaultResponse(false);
            var request = new LoginRequest();
            try {
                request = JsonConvert.DeserializeObject<LoginRequest>(body.ToString());
                request.Email = request.Email.Encode().Varchar(128);
                request.Password = request.Password.ToMd5();
            }catch (Exception ex) {
                return JsonConvert.SerializeObject(output);
            }
            var users = Data.SelectUsers(new List<QueryObject>() {
                new QueryObject("EMAIL",request.Email,true)
            });
            var user = new User() {
                Email = request.Email,
                Password = request.Password,
                Name = "User"
            };
            if (users.Any()) /*->*/ user = users[0]; else Data.CreateUser(user);
            if (user.Email.Equals(request.Email) && user.Password.Equals(request.Password)) {
                output.Response = true;
            }
            return JsonConvert.SerializeObject(output);
        }

        [HttpPost("/api/getuser")]
        public string OnGetUser([FromBody] object body) {
            dynamic output = new DefaultResponse(false);
            string beforeHashing;
            var request = new LoginRequest();
            try {
                request = JsonConvert.DeserializeObject<LoginRequest>(body.ToString());
                request.Email = request.Email.Encode().Varchar(128);
                beforeHashing = request.Password;
                request.Password = request.Password.ToMd5();
            }catch (Exception ex) {
                return JsonConvert.SerializeObject(output);
            }
            var users = Data.SelectUsers(new List<QueryObject>() {
                new QueryObject("EMAIL",request.Email,true),
                new QueryObject("PASSWORD",request.Password,true)
            });
            try {
                users[0].Email = users[0].Email.Decode();
                users[0].Name = users[0].Name.Decode();
                users[0].Password = beforeHashing;
                output = users[0];
            }catch(Exception) {}
            return JsonConvert.SerializeObject(output);
        }

        [HttpPost("/api/updateuser")]
        public string OnUpdateUser([FromBody] object body) {
            var output = new DefaultResponse(true);
            try {
                var userRequest = JsonConvert.DeserializeObject<UpdateUserRequest>(body.ToString());
                userRequest.Email = userRequest.Email.Encode().Varchar(128);
                userRequest.Password = userRequest.Password.ToMd5();
                userRequest.User.Email = userRequest.Email.Encode().Varchar(128);
                userRequest.User.Name = userRequest.User.Name.Encode().Varchar(32);
                userRequest.User.Password = userRequest.User.Password.ToMd5();
                var users = Data.SelectUsers(new List<QueryObject>() {
                    new QueryObject("EMAIL",userRequest.Email,true),
                    new QueryObject("PASSWORD",userRequest.Password,true)
                });
                if (users.Any()) {
                    var user = users[0];
                    userRequest.User.Id = user.Id;
                    Data.UpdateUser(new QueryObject() {
                        Key = "ID",
                        Value = user.Id.ToString(),
                        IsString = false
                    }, new List<QueryObject>() {
                        new QueryObject() {
                            Key = "NAME",
                            Value = userRequest.User.Name,
                            IsString = true
                        },
                        new QueryObject() {
                            Key = "PASSWORD",
                            Value = userRequest.User.Password,
                            IsString = true
                        }
                    });
                }else {
                    output.Response = false;
                }
            }catch (Exception) {
                output.Response = false;
            }
            return JsonConvert.SerializeObject(output);
        }
        
        [HttpPost("/api/deleteuser")]
        public string OnDeleteUser([FromBody] object body) {
            var output = new DefaultResponse(true);
            try {
                var request = JsonConvert.DeserializeObject<LoginRequest>(body.ToString());
                request.Email = request.Email.Encode().Varchar(128);
                request.Password = request.Password.ToMd5();
                var users = Data.SelectUsers(new List<QueryObject>() {
                    new QueryObject("EMAIL",request.Email,true),
                    new QueryObject("PASSWORD",request.Password,true)
                });
                if (users.Any()) {
                    var user = users[0];
                    Data.DeleteUser(new List<QueryObject>() {
                        new QueryObject("EMAIL",user.Email,true)
                    });
                }
                else {
                    output.Response = false;
                }
            }
            catch (Exception) {
                output.Response = false;
            }
            return JsonConvert.SerializeObject(output);
        }
    }
}