﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

using static Shihonase.Utils;
namespace Shihonase.Controllers {
    
    [Route("[controller]")]
    [ApiController]
    public class TaskController {
        [HttpPost("/api/gettask")]
        public string OnGetTask([FromBody] object body) {
            var output = new List<Task>();
            
            try {
                var request = JsonConvert.DeserializeObject<GetTasksRequest>(body.ToString());
                request.Email = request.Email.Encode().Varchar(128);
                request.Password = request.Password.ToMd5();
                var users = Data.SelectUsers(new List<QueryObject>() {
                    new QueryObject("EMAIL",request.Email,true),
                    new QueryObject("PASSWORD",request.Password,true)
                });
                if (users.Any()) {
                    var user = users[0];
                    if (DataUtils.CategoryBelongsToUser(request.Category_Id,user.Id)) {
                        output = Data.SelectTasks(new List<QueryObject>() {
                            new QueryObject() {
                                Key = "CATEGORY_ID",
                                Value = request.Category_Id.ToString(),
                                IsString = false
                            }
                        });
                    }
                }
            }
            catch (Exception) {}

            return JsonConvert.SerializeObject(output);
        }
        
        [HttpPost("/api/addtask")]
        public string OnAddTask([FromBody] object body) {
            var output = new DefaultResponse(true);

            try {
                var request = JsonConvert.DeserializeObject<AddTaskRequest>(body.ToString());
                request.Email = request.Email.Encode().Varchar(128);
                request.Password = request.Password.ToMd5();
                var users = Data.SelectUsers(new List<QueryObject>() {
                    new QueryObject("EMAIL",request.Email,true),
                    new QueryObject("PASSWORD",request.Password,true)
                });
                if (users.Any()) {
                    if (DataUtils.CategoryBelongsToUser(request.Task.Category_Id, users[0].Id)) {
                        Data.CreateTask(request.Task);
                    }
                } else output.Response = false;
            }
            catch (Exception ex) {
                output.Response = false;
            }

            return JsonConvert.SerializeObject(output);
        }
        
        [HttpPost("/api/updatetasks")]
        public string OnUpdateTasks([FromBody] object body) {
            var output = new DefaultResponse(true);

            try {
                var request = JsonConvert.DeserializeObject<UpdateTasksRequest>(body.ToString());
                request.Email = request.Email.Encode().Varchar(128);
                request.Password = request.Password.ToMd5();
                var users = Data.SelectUsers(new List<QueryObject>() {
                    new QueryObject("EMAIL",request.Email,true),
                    new QueryObject("PASSWORD",request.Password,true)
                });
                if (users.Any()) {
                    var user = users[0];
                    request.Tasks.ForEach(o => {
                        if (DataUtils.CategoryBelongsToUser(o.Category_Id, user.Id)) {
                            //DEADLINE, STATE, NAME, COLOR, POSITION, DESCRIPTION
                            Data.UpdateTask(new QueryObject() {
                                Key = "ID",
                                Value = o.Id.ToString(),
                                IsString = false
                            },new List<QueryObject>() {
                                new QueryObject() {
                                    Key = "NAME",
                                    Value = o.Name,
                                    IsString = true
                                },
                                new QueryObject() {
                                    Key = "COLOR",
                                    Value = o.Color,
                                    IsString = true
                                },
                                new QueryObject() {
                                    Key = "POSITION",
                                    Value = o.Position.ToString(),
                                    IsString = false
                                },
                                new QueryObject() {
                                    Key = "DESCRIPTION",
                                    Value = o.Description,
                                    IsString = true
                                },
                                new QueryObject() {
                                    Key = "DEADLINE",
                                    Value = o.Deadline,
                                    IsString = false,
                                    IsDateTime = true
                                },
                                new QueryObject() {
                                    Key = "STATE",
                                    Value = o.State,
                                    IsString = false
                                },
                            });
                        }
                    });
                }else {
                    output.Response = false;
                }
            }
            catch (Exception ex) {
                output.Response = false;
            }

            return JsonConvert.SerializeObject(output);
        }
        
        [HttpPost("/api/deletetask")]
        public string OnDeleteTask([FromBody] object body) {
            var output = new DefaultResponse(true);

            try {
                var request = JsonConvert.DeserializeObject<AddTaskRequest>(body.ToString());
                request.Email = request.Email.Encode().Varchar(128);
                request.Password = request.Password.ToMd5();
                var users = Data.SelectUsers(new List<QueryObject>() {
                    new QueryObject("EMAIL",request.Email,true),
                    new QueryObject("PASSWORD",request.Password,true)
                });
                if (users.Any()) {
                    if (DataUtils.CategoryBelongsToUser(request.Task.Category_Id, users[0].Id)) {
                        Data.DeleteTask(new List<QueryObject>() {
                            new QueryObject() {
                                Key = "ID",
                                Value = request.Task.Id.ToString(),
                                IsString = false
                            }
                        });
                    }
                }else {
                    output.Response = false;
                }
            }
            catch (Exception) {
                output.Response = false;
            }

            return JsonConvert.SerializeObject(output);
        }
    }
}