﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

using static Shihonase.Utils;
namespace Shihonase.Controllers {
    [Route("[controller]")]
    [ApiController]
    public class CategoryController {
        [HttpPost("/api/getcategories")]
        public string OnGetCategories([FromBody] object body) {
            var output = new List<Category>();

            try {
                var request = JsonConvert.DeserializeObject<LoginRequest>(body.ToString());
                request.Email = request.Email.Encode().Varchar(128);
                request.Password = request.Password.ToMd5();
                var users = Data.SelectUsers(new List<QueryObject>() {
                    new QueryObject("EMAIL",request.Email,true),
                    new QueryObject("PASSWORD",request.Password,true)
                });
                if (users.Any()) {
                    var user = users[0];
                    output = Data.SelectCategories(new List<QueryObject>() {
                        new QueryObject() {
                            Key = "USER_ID",
                            Value = user.Id.ToString(),
                            IsString = false
                        }
                    });
                }
            }
            catch (Exception) {}

            return JsonConvert.SerializeObject(output);
        }

        [HttpPost("/api/addcategory")]
        public string OnAddCategory([FromBody] object body) {
            var output = new DefaultResponse(true);

            try {
                var request = JsonConvert.DeserializeObject<AddCategoryRequest>(body.ToString());
                request.Email = request.Email.Encode().Varchar(128);
                request.Password = request.Password.ToMd5();
                var users = Data.SelectUsers(new List<QueryObject>() {
                    new QueryObject("EMAIL",request.Email,true),
                    new QueryObject("PASSWORD",request.Password,true)
                });
                if (users.Any()) {
                    var user = users[0];
                    request.Category.User_Id = user.Id;
                    Data.CreateCategory(request.Category);
                }else {
                    output.Response = false;
                }
            }
            catch (Exception) {
                output.Response = false;
            }

            return JsonConvert.SerializeObject(output);
        }

        [HttpPost("/api/updatecategories")]
        public string OnUpdateCategories([FromBody] object body) {
            var output = new DefaultResponse(true);

            try {
                var request = JsonConvert.DeserializeObject<UpdateCategoriesRequest>(body.ToString());
                request.Email = request.Email.Encode().Varchar(128);
                request.Password = request.Password.ToMd5();
                var users = Data.SelectUsers(new List<QueryObject>() {
                    new QueryObject("EMAIL",request.Email,true),
                    new QueryObject("PASSWORD",request.Password,true)
                });
                if (users.Any()) {
                    var user = users[0];
                    request.Categories.ForEach(o => {
                        Data.UpdateCategory(new QueryObject() {
                            Key = "ID",
                            Value = o.Id.ToString(),
                            IsString = false
                        },new List<QueryObject>() {
                            new QueryObject() {
                                Key = "NAME",
                                Value = o.Name,
                                IsString = true
                            },
                            new QueryObject() {
                                Key = "COLOR",
                                Value = o.Color,
                                IsString = true
                            },
                            new QueryObject() {
                                Key = "POSITION",
                                Value = o.Position.ToString(),
                                IsString = false
                            },
                            new QueryObject() {
                                Key = "DESCRIPTION",
                                Value = o.Description,
                                IsString = true
                            },
                        });
                    });
                }else {
                    output.Response = false;
                }
            }
            catch (Exception ex) {
                output.Response = false;
            }

            return JsonConvert.SerializeObject(output);
        }
        
        [HttpPost("/api/deletecategory")]
        public string OnDeleteCategory([FromBody] object body) {
            var output = new DefaultResponse(true);

            try {
                var request = JsonConvert.DeserializeObject<AddCategoryRequest>(body.ToString());
                request.Email = request.Email.Encode().Varchar(128);
                request.Password = request.Password.ToMd5();
                var users = Data.SelectUsers(new List<QueryObject>() {
                    new QueryObject("EMAIL",request.Email,true),
                    new QueryObject("PASSWORD",request.Password,true)
                });
                if (users.Any()) {
                    if (DataUtils.CategoryBelongsToUser(request.Category.Id, users[0].Id)) {
                        Data.DeleteCategory(new List<QueryObject>() {
                            new QueryObject() {
                                Key = "ID",
                                Value = request.Category.Id.ToString(),
                                IsString = false
                            }
                        });
                    }
                }else {
                    output.Response = false;
                }
            }
            catch (Exception) {
                output.Response = false;
            }

            return JsonConvert.SerializeObject(output);
        }
    }
}