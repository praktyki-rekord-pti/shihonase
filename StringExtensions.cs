﻿using System.Text;
using System.Text.RegularExpressions;

namespace Shihonase {
    public static class StringExtensions {
        public static string ToMd5(this string str)
        {
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = Encoding.UTF8.GetBytes(str);
                byte[] hashBytes = md5.ComputeHash(inputBytes);
                
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }
        
        public static string Encode(this string str) => System.Web.HttpUtility.HtmlEncode(str);
        
        public static string Decode(this string str) => System.Web.HttpUtility.HtmlDecode(str);

        public static string SqlEscape(this string str) => str.Replace("'", @"''");
        
        public static string Nvarchar(this string str, int size) {
            if (str.Length > size) str = str.Substring(0, size);
            return str;
        }

        public static string Varchar(this string str, int size) =>
            Regex.Replace(Nvarchar(str, size), @"[^\u0000-\u007F]+", string.Empty);
    }
}