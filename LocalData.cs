﻿using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using MeowUtils.Database;

using static Shihonase.Utils;

namespace Shihonase {
    public static class LocalData {
        public static readonly Database Db = new Database("config.db");

        public static void Setup() {
            var map = new Dictionary<string, string>() {
                {"Server",""},
                {"DatabaseName",""},
                {"User",""},
                {"Password",""},
            };
            foreach (var kv in map) /*->*/ map[kv.Key] = Ask($"{kv.Key}:");
            Db.CreateIfNotExists();
            Db.NewQuery("PRAGMA journal_mode=WAL;", Database.ResponseType.Null); //allows multiple queries at once
            Db.NewQuery(@"create table if not exists config(
  id integer primary key,
  name text unique not null,
  value text not null
);", Database.ResponseType.Null);
            foreach (var kv in map) /*->*/ SetValue(kv.Key, kv.Value);
        }

        public static void OnStart() {
            Db.HandleExceptions = true;
            if (!File.Exists(Db.Name)) Setup();
            Data.ConnectionData.Server = GetValue("Server");
            Data.ConnectionData.DbName = GetValue("DatabaseName");
            Data.ConnectionData.User = GetValue("User");
            Data.ConnectionData.Pass = GetValue("Password");
        }

        public static void SetValue(string valueName, string value) {
            Db.NewQuery(
                $"insert or replace into config(name, value)values('{valueName}','{value}');", 
                Database.ResponseType.Null
                );
        }

        public static string GetValue(string valueName) {
            var output = string.Empty;
            var result = (SQLiteDataReader) Db.NewQuery(
                $"select * from config where name='{valueName}';",
                Database.ResponseType.SqliteDataReader
                );
            while (result.Read()) { 
                output = result.GetString(result.GetOrdinal("value"));
                break;
            }
            return output;
        }

    }
}