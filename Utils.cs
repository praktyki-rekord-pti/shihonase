﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Shihonase {
    public class Utils {
        public static string Ask(string message) {
            Console.Write(message);
            return Console.ReadLine();
        }

        public static string AskLine(string message) => $"{Ask(message)}\n";
        
        public static string DateTimeToSmallDateTime(DateTime dt) {
            var output = string.Empty;
            output = dt.ToString("yyyy-MM-dd HH:mm:ss");
            return output;
        }

        public static string Nvarchar(string str, int size) {
            if (str.Length > size) str = str.Substring(0, size);
            return str;
        }

        public static string Varchar(string str, int size) =>
            Regex.Replace(Nvarchar(str, size), @"[^\u0000-\u007F]+", string.Empty);
        

        public static string Encode(string str) => System.Web.HttpUtility.HtmlEncode(str);
        
        public static string Decode(string str) => System.Web.HttpUtility.HtmlDecode(str);
    }
}